package com.cliimbheads.contactappwa.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cliimbheads.contactappwa.Model.ContactModel;
import com.cliimbheads.contactappwa.R;

import java.util.List;

/**
 * Created by maekesh on 02/07/18.
 */


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContentHolder> {


    private List<ContactModel> contactModels;
    private Context mContext;

    public ContactAdapter(List<ContactModel> contactModels, Context mContext) {
        this.mContext = mContext;
        this.contactModels = contactModels;
    }




    @NonNull
    @Override
    public ContentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_contact_display, null);
        ContentHolder contentHolder = new ContentHolder(view);
        return contentHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContentHolder holder, int position) {

        ContactModel contactModel = contactModels.get(position);
        holder.contactName.setText(contactModel.getContactName());
        holder.contactNumber.setText(contactModel.getContactNumber());

    }

    @Override
    public int getItemCount() {
        return contactModels.size();
    }

    public static class ContentHolder extends RecyclerView.ViewHolder{

        TextView contactName;
        TextView contactNumber;


        public ContentHolder(View itemView) {
            super(itemView);

            contactName = (TextView) itemView.findViewById(R.id.txContactName);
            contactNumber = (TextView) itemView.findViewById(R.id.txContactNumber);
        }
    }
}
