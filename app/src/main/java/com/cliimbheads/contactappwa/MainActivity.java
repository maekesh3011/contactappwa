package com.cliimbheads.contactappwa;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.cliimbheads.contactappwa.Adapter.ContactAdapter;
import com.cliimbheads.contactappwa.Model.ContactModel;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView contentList;
    AVLoadingIndicatorView progressBar;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contentList = (RecyclerView) findViewById(R.id.rvContacts);
        progressBar = (AVLoadingIndicatorView) findViewById(R.id.avi);

        checkPermission();
    }

    private void checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            LoadContactAsync loadContactAsync = new LoadContactAsync();
            loadContactAsync.execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                LoadContactAsync loadContactAsync = new LoadContactAsync();
                loadContactAsync.execute();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //checkPermission();
    }

    public class LoadContactAsync extends AsyncTask<Void, Void, List<ContactModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.smoothToShow();
        }

        @Override
        protected List<ContactModel> doInBackground(Void... voids) {

            List<ContactModel> contactModelsList = new ArrayList<>();
            ContactModel contactModel;
            ContentResolver contentResolver = getContentResolver();

            //RowContacts for filter Account Types
            Cursor contactCursor = contentResolver.query(
                    ContactsContract.RawContacts.CONTENT_URI,
                    new String[]{ContactsContract.RawContacts._ID,
                            ContactsContract.RawContacts.CONTACT_ID},
                    ContactsContract.RawContacts.ACCOUNT_TYPE + "= ?",
                    new String[]{"com.whatsapp"},
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

            if (contactCursor != null) {
                if (contactCursor.getCount() > 0) {
                    if (contactCursor.moveToFirst()) {
                        do {

                            //whatsappContactId for get Number,Name,Id ect... from  ContactsContract.CommonDataKinds.Phone
                            String whatsappContactId = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));

                            if (whatsappContactId != null) {
                                //Get Data from ContactsContract.CommonDataKinds.Phone of Specific CONTACT_ID
                                Cursor whatsAppContactCursor = contentResolver.query(
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                        new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                        new String[]{whatsappContactId}, null);

                                if (whatsAppContactCursor != null) {
                                    whatsAppContactCursor.moveToFirst();
                                    String id = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                                    String name = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                    String number = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    whatsAppContactCursor.close();

                                    //Add Number to ArrayList
                                    contactModel = new ContactModel();
                                    contactModel.setContactName(name);
                                    contactModel.setContactNumber(number);
                                    contactModelsList.add(contactModel);
                                }
                            }
                        } while (contactCursor.moveToNext());
                        contactCursor.close();
                    }
                }
            }
            return contactModelsList;
        }

        @Override
        protected void onPostExecute(List<ContactModel> contactModels) {
            super.onPostExecute(contactModels);

            ContactAdapter contactAdapter = new ContactAdapter(contactModels, getApplicationContext());
            contentList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            contentList.setAdapter(contactAdapter);
            progressBar.smoothToHide();
            progressBar.setVisibility(View.GONE);
        }
    }
}
