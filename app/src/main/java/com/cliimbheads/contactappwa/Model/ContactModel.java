package com.cliimbheads.contactappwa.Model;

/**
 * Created by maekesh on 02/07/18.
 */

public class ContactModel {

    private String contactName;
    private String contactNumber;


    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
